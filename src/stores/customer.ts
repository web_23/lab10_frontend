import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import customerService from "@/services/customer";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({
    name: "",
    age: 11,
    tel: "",
    gender: "",
  });
  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = {
        name: "",
        age: 11,
        tel: "",
        gender: "",
      };
    }
  });
  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomer();
      customers.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }
      dialog.value = false;
      await getCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }
  async function deleteC(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.deleteCustomer(id);
      await getCustomers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    customers,
    getCustomers,
    editCustomer,
    dialog,
    editedCustomer,
    deleteC,
    saveCustomer,
  };
});
