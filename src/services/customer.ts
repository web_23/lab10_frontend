import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomer() {
  return http.get("/customers");
}
function saveCustomer(customer: Customer) {
  return http.post("/customers", customer);
}
function updateCustomer(id: number, customer: Customer) {
  return http.patch("/customers/" + id, customer);
}
function deleteCustomer(id: number) {
  return http.delete("/customers/" + id);
}

export default { getCustomer, saveCustomer, updateCustomer, deleteCustomer };
