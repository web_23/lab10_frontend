export default interface Customer {
  id?: number;
  name: string;
  age: number;
  tel: string;
  gender: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
